﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class green_laserbullet : MonoBehaviour
{
    public Rigidbody2D laserbullet;
    public float speed;
    // Start is called before the first frame update
    void Start()
        
    {
        laserbullet.AddForce(transform.up * speed);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
