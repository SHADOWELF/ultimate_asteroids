﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class game_manager : MonoBehaviour
{
    public static game_manager instance;

    [Header("player")]
    public GameObject player;


    [Header("spawning")]
    public GameObject spawner;
    public int spawnMax;

    [Header("asteroids")]
    public List<GameObject> asteroidsList;
    public GameObject asteroidsPrefab;


    private void Awake()
    {
        if (instance !=null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (asteroidsList.Count < spawnMax)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        spawner.GetComponent<spawner_script>().Move();
        GameObject asteroids= Instantiate(asteroidsPrefab, spawner.transform.position, spawner.transform.rotation);
        asteroidsList.Add(asteroids);
    }
}
