﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    public float rotateSpeed;
    public float speed;
    public GameObject laserbullet;
    public Transform laserbarrel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // this gets the input from the player from horizontal and vertical
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        //rotate the ship if the player enters horizontal input
        //z axis turns left positive and right negative(left and right)
        transform.Rotate(transform.forward * horizontal * Time.deltaTime * -1 * rotateSpeed);

        //move ship forward or backwards if play enters vertical input
        transform.Translate(Vector3.up * vertical * speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {

            Instantiate(laserbullet, laserbarrel.position, laserbarrel.rotation);

        }

    }
}
