﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner_script : MonoBehaviour
{
    public float radius;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Move()
    {
        Vector3 newPos = Random.insideUnitCircle * radius;
        transform.position = newPos;
    }

}
